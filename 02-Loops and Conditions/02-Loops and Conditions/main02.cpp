// Loops and Conditional Statements.
#include <iostream>

int main()
{
	int input = 0;
	int sum = 0;
	while (input != 5)
	{
		std::cout << "Menu Selection:" << std::endl;
		std::cout << "     (1) Addition" << std::endl;
		std::cout << "     (2) Subtraction" << std::endl;
		std::cout << "     (3) Multiplication" << std::endl;
		std::cout << "     (4) Division" << std::endl;
		std::cout << "     (5) Exit" << std::endl;
		std::cin >> input;
		std::cout << "\nFirst Number";
		std::cin >> sum;

		if (input == 1)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum + input;
				std::cout << "\nCurrent sum: " << sum << std::endl;
			}
		}
		if (input == 2)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum - input;
				std::cout << "\nCurrent sum: " << sum << std::endl;
			}
		}
		if (input == 3)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum * input;
				std::cout << "\nCurrent total: " << sum << std::endl;
			}
		}
		if (input == 4)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum / input;
				std::cout << "\nCurrent total: " << sum << std::endl;
			}
		}
		std::cout << "\n Final Value: " << sum;
	}
	return 0;
}

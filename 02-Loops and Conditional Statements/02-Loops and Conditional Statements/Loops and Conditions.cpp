#include <iostream>

int main()
{
	int input = 0;
	int sum = 0;
	while(input != 5)
	{
		std::cout << "Menu Selection:" << std::endl;
		std::cout << "(1) Addition" << std::endl;
		std::cout << "(2) Subtraction" << std::endl;
		std::cout << "(3) Multiplication" << std::endl;
		std::cout << "(4) Division" << std::endl;
		std::cin >> input;
		std::cout << "\nFirst Number:" << std::endl;
		std::cin >> sum;

		if (input == 1)
		{
			while (input != 0) 
			{
				std::cin >> input;
				sum = sum + input;
				std::cout << "\nCurrent Sum: " << sum << std::endl;
			}
		}
		if (input == 2)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum - input;
				std::cout << "\nCurrent Total: " << sum << std::endl;
			}
		}
		if (input == 3)
		{
			while (input != 0)
			{
				std::cin >> input;
				sum = sum * input;
				std::cout << "\nCurrent Total: " << sum << std::endl;
			}
		}
		if (input == 4)
		{
			while (input != 0)
			{
				std::cin >> input;
				if(input != 0)
					sum = sum / input;
				std::cout << "\nCurrent Total: " << sum << std::endl;
			}
		}
	}
	return 0;
}

/*
Conditional Statements:
==, !=, >, <, <=, >=, &&, ||

while(condition == true)
{
 //Code to Execute
}

do {
//Code to Execute
} while(condition == true);

for(int i = 0; i < 10; ++i)
{
 // Code to Execute
}

*/
#include <iostream>
#include <cstdio>

/*
int main()
{
	int one, two;
	do
		{
		std::cout << "Input two numbers to add: ";
		std::cin >> one;
		std::cin >> two;
		std::cout << "Hi Mom! I can add : " << one + two 
			<< std::endl;
	} while(one != 0 || two != 0);
	return 0;
}
*/
/*
printf(const char * format string, arguments, ...)

int main()
{
	int one, two;
	do 
	{
		printf("%s", "Input two number to add:");
		scanf_s("%d", &one);
		scanf_s("%d", &two);
		printf("%s%d%s", "Hi Mom! I can add : ", one + two, "\n");
	} while (one != 0 || two != 0);
	return 0;
}
*/
int main()
{
	int one, two;
	char buffer[11];
	do 
	{
		fputs("Input two numbers to add:", stdout);
		fgets(buffer, 11, stdin);
		one = atoi(buffer);
		fgets(buffer, 11, stdin);
		two = atoi(buffer);
		fputs("Hi Mom! I can add! : ", stdout);
		_itoa_s(one + two, buffer, 11);
		puts(buffer);
	} while (one != 0 || two != 0);
	return 0;
}